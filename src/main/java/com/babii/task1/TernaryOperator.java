package com.babii.task1;

@FunctionalInterface
public interface TernaryOperator {
    int apply(int first, int second, int third);
}
