package com.babii.task1;

import java.util.Scanner;
import java.util.function.IntBinaryOperator;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int first, second, third;
        System.out.print("first = ");
        first = scanner.nextInt();
        System.out.print("second = ");
        second = scanner.nextInt();
        System.out.print("third = ");
        third = scanner.nextInt();

        System.out.println("Max = " + getMax(first, second, third));
        System.out.println("Average = " + getAverage(first, second, third));
        scanner.close();
    }

    public static int getMax(int first, int second, int third) {
        IntBinaryOperator maxOfTwo = (a, b) -> (a > b) ? a : b;
        TernaryOperator maxOfThree = (a, b, c) ->
                maxOfTwo.applyAsInt(maxOfTwo.applyAsInt(a, b), c);

        return maxOfThree.apply(first, second, third);
    }

    public static int getAverage(int first, int second, int third) {
        TernaryOperator averageOfThree = (a, b, c) -> (a + b + c) / 3;
        return averageOfThree.apply(first, second, third);
    }
}
