package com.babii.task2;

public class Close implements Command{
    public static final String COMMAND_NAME = "Close";
    @Override
    public void execute(String argument) {
        System.out.println("Close executed with argument " + argument);
    }
}
