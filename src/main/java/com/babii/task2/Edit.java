package com.babii.task2;

public class Edit implements Command{
    public static final String COMMAND_NAME = "Edit";
    @Override
    public void execute(String argument) {
        System.out.println("Edit executed with argument " + argument);
    }
}
