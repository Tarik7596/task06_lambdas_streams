package com.babii.task2;

@FunctionalInterface
public interface Open extends Command {
    String COMMAND_NAME = "Open";
}
