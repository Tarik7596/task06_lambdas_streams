package com.babii.task2;

public interface Command {
    public void execute(String argument);
}
