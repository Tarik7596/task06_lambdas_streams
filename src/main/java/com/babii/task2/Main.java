package com.babii.task2;

import java.util.Scanner;

public class Main {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        start();
    }

    public static void start(){
        while(true){
            System.out.println("\nEnter command to execute");
            System.out.println("Open, Edit, Save, Close");
            String command = SCANNER.next();

            System.out.print("enter argument: ");
            String argument = SCANNER.next();

            switch(command){
                case Open.COMMAND_NAME:
                    Command openCommand = (p)->System.out.println("Open executed with argument " + argument);
                    openCommand.execute(argument);
                    break;
                case Edit.COMMAND_NAME:
                    Command editCommand = new Edit()::execute;
                    editCommand.execute(argument);
                    break;
                case Save.COMMAND_NAME:
                    Command saveCommand = new Save() {
                        @Override
                        public void execute(String argument) {
                            System.out.println("Save executed with argument " + argument);
                        }
                    };
                    saveCommand.execute(argument);
                    break;
                case Close.COMMAND_NAME:
                    Command closeCommand = new Close();
                    closeCommand.execute(argument);
                    break;
                default:
                    System.out.println("Incorrect command");
            }
        }
    }
}
