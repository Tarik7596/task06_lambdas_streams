package com.babii;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Task3 {
    private static final Random random = new Random();

    public static void main(String[] args) {

        printInfo(getListUsingGenerate());
        printInfo(getListUsingIterate());
        printInfo(getListUsingRange());
    }

    public static List<Integer> getListUsingGenerate() {
        return Stream.generate(() -> random.nextInt(30)).limit(10).collect(Collectors.toList());
    }
    public static List<Integer> getListUsingIterate() {
        return Stream.iterate(2, e -> e + random.nextInt(10)).limit(10).collect(Collectors.toList());
    }

    public static List<Integer> getListUsingRange() {
        return IntStream.rangeClosed(10, 20).map(e -> e * random.nextInt(5)).mapToObj(Integer::valueOf)
                .collect(Collectors.toList());
    }

    public static void printInfo(List<Integer> list) {
        double average = list.stream().collect(Collectors.averagingInt(p->p));

        int min = list.stream().min(Integer::compareTo).get();
        int max = list.stream().max(Integer::compareTo).get();

        IntSummaryStatistics stat = list.stream().mapToInt(p -> p).summaryStatistics();
        int sum = (int) stat.getSum();

        int sumReduce = list.stream().reduce((a, b) -> (a + b)).get();
        int cntGreaterThanAvrg = (int) list.stream().filter(p -> p > average).count();

        System.out.println(list);
        System.out.println("average=" + average + ", min=" + min + ", max=" + max + ", sum=" + sum);
        System.out.println("sum(reduce)=" + sumReduce);
        System.out.println("count of elements greater than average=" + cntGreaterThanAvrg);
        System.out.println();
    }
}
