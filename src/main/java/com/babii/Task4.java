package com.babii;

import java.util.*;
import java.util.stream.Collectors;

public class Task4 {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        LinkedList<String> lines = (LinkedList<String>) readAndGetLines();

        List<String> words = lines.stream()
                .flatMap(p -> Arrays.asList(p.split("\\W+"))
                        .stream())
                .collect(Collectors.toList());
        System.out.println("All words(count=" + words.size() + ")");
        System.out.println(words + "\n");

        List<String> sortedUnqWords = words
                .stream().distinct()
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toList());
        System.out.println("Sorted unique words(count=" + sortedUnqWords.size() + ")");
        System.out.println(sortedUnqWords + "\n");

        String wordCount = words
                .stream()
                .collect(Collectors.groupingBy(p -> p, Collectors.counting())).entrySet()
                .stream()
                .map(p -> p.getKey() + "-" + p.getValue() + "\n")
                .collect(Collectors.joining(" "));

        System.out.println("Word-count");
        System.out.println(wordCount);

        Map<String, Long> symbolCount = lines
                .stream()
                .flatMap(p -> Arrays.asList(p.split(""))
                        .stream())
                .filter(p -> !Character.isUpperCase(p.charAt(0)))
                .collect(Collectors.groupingBy(p -> p, Collectors.counting()));

        System.out.println("Symbol - count");
        for (Map.Entry<String, Long> entry : symbolCount.entrySet())
            System.out.println("\'" + entry.getKey() + "\'" + " - " + entry.getValue());

    }

    public static List<String> readAndGetLines() {
        LinkedList<String> lines = new LinkedList<>();
        int i = 0;
        System.out.println("Enter lines, empty line to exit");
        while (true) {
            System.out.print("enter line[" + i++ + "]: ");
            String current = SCANNER.nextLine();
            if (current.equals("")) {
                System.out.println("exit entering lines\n\n");
                break;
            } else
                lines.add(current);
        }
        return lines;
    }
}
